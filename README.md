# See the Slots - Book the Spots

This repository contains the code for the assignment in course Algorithms for Decision Support.
There are two folders named offline_case and online_case for the offline and the online case respectively.

## Installation

Make sure Python 3.8 is installed on your machine.

Also make sure that the following packages are installed
```python
pip install glob
pip install matplotlib
pip install pandas
```

## Offline Case

To run any offline case drop the (offline) test case instances into *Test_Cases/Offline* folder and then run *offline_case/main.py*. 

If you want to run a specific test case instance the function *compute_specific_case(filename)* can do so. For example *compute_specific_case("45.txt")* computes the test case with the filename *45.txt*.

The results are then published in *Test_Cases/Offline/Output* folder with a filename of *solution_of_filename.txt* where filename denotes the case you ran previously.  
If everything went well, the passed test cases will be then moved automatically to *Test_Cases/Offline/passed_cases* and the metrics will be published in *Test_Cases/Offline/Output/Memory_Usage*. 

To see the results analysis run the *offline_case/results_analysis.py* and comment / uncomment the desired function to plot.  
There are 3 different plot functions; *draw_subplots(), plot_time_intervals(), plot_memory_intervals()*. Due to some limitation with how Windows directories work you may come up with an issue when two of them are run in the same iteration of the program. Thus, we advise you to run them separately.

## Online Case
To run the online case either run *online_case/main.py* which will run by default the Sequential Planner (refer to the paper for more details) or specify the Integrated vaccines planner with the flag of *-p I*  
To run a specific case specify it with the flag of *-f fileName*.
To run a performance benchmark on randomly generated instances use the flag *-i benchmark*

## Authors
Athanasios Tsiamis

Julian Markus

Leona Teunissen

Ramón R. Cuevas

## Contact

Contact with any of the authors is more than welcome.  
Drop us an email or a message in MS-Teams and we will try to help you as best as we can.