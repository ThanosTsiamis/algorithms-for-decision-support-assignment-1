import errno
import random
import os
import uuid


def create_random_test_case_instances(nr_of_files_to_be_created=1, nr_of_patients=None, processing_time_1=None,
                                      processing_time_2=None,
                                      gap=None, first_available_time_slot=None, length_first_timeslot=None, delay=None,
                                      length_of_second_interval=None):
    for foo in range(nr_of_files_to_be_created):
        if nr_of_patients is None:
            nr_of_patients = random.randint(2, 50)
        if processing_time_1 is None:
            processing_time_1 = random.randint(1, 40)
        if processing_time_2 is None:
            processing_time_2 = random.randint(1, 40)
        if gap is None:
            gap = random.randint(1, 400)
        file_uuid = uuid.uuid1()
        filename = "../Test_Cases/Offline/" + str(nr_of_patients) + "-" + str(file_uuid) + ".txt"
        if not os.path.exists(os.path.dirname(filename)):
            try:
                os.makedirs(os.path.dirname(filename))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        with open(filename, "a") as file:
            file.write(str(processing_time_1) + "\n")
            file.write(str(processing_time_2) + "\n")
            file.write(str(gap) + "\n")
            file.write(str(nr_of_patients) + "\n")
            for patient_nr in range(nr_of_patients):
                first_available_time_slot = random.randint(1, 10)
                last_available_time_slot = first_available_time_slot + processing_time_1 - 1
                delay = random.randint(1, 10)
                length_of_second_interval = processing_time_2

                if patient_nr != nr_of_patients - 1:
                    file.write(
                        str(first_available_time_slot) + "," + str(last_available_time_slot) + "," + str(
                            delay) + "," + str(
                            length_of_second_interval) + "\n")
                else:
                    file.write(
                        str(first_available_time_slot) + "," + str(last_available_time_slot) + "," + str(
                            delay) + "," + str(
                            length_of_second_interval))
            file.close()
