import os
import time

import psutil as psutil
from ortools.sat.python import cp_model
import numpy as np
import shutil


def read_from_file(filename):
    with open(filename) as file:
        lines = file.read().splitlines()
        processing_time_1 = int(lines[0])
        processing_time_2 = int(lines[1])
        g = int(lines[2])
        n = int(lines[3])
        temp = lines[4:]
        patient_info = []
        for item in temp:
            patient_info.append(item.rstrip().split(","))
        patient_info = [list(map(int, i)) for i in patient_info]
        file.close()

        return processing_time_1, processing_time_2, g, n, patient_info


def find_range_of_T(patient_info, gap):
    patient_info = np.array(patient_info)
    T = range(max(patient_info[:, 1] + patient_info[:, 2] + patient_info[:, 3]) + 1 + gap)
    return T


def write_to_file_patient_list(filename, timeslot_dose_1, hospital_dose_1, timeslot_dose_2, hospital_dose_2):
    with open("Output/solution_of_" + filename, 'a') as file:
        file.write(str(timeslot_dose_1) + "," + str(hospital_dose_1) + "," + str(timeslot_dose_2) + "," + str(
            hospital_dose_2) + "\n")


def write_to_file_hospitals_needed(filename, amount_of_hospitals_needed):
    with open("Output/solution_of_" + filename, 'a') as file:
        file.write(str(amount_of_hospitals_needed))


def write_memory_used(filename, process, memory):
    with open("Output/Memory_Usage/metrics_of_" + filename, 'a') as file:
        file.write(process + "," + str(memory) + "\n")


def write_time_used(filename, process, time):
    with open("Output/Memory_Usage/Time_of_" + filename, 'a') as file:
        file.write(process + "," + str(time) + "\n")


def move_file_to_passed_cases(filename):
    shutil.move(filename, "passed_cases/" + filename)


def ilp_solver(filename):
    start = time.perf_counter()
    process = psutil.Process(os.getpid())
    # Creates the model.
    model = cp_model.CpModel()

    # Read the data from the file.
    p1, p2, g, n, patient_info = read_from_file(filename=filename)
    if n == 0:
        write_to_file_patient_list(filename, 0, 0, 0, 0)
        write_to_file_hospitals_needed(filename, 0)
        return 0
    N = n
    n = range(n)
    T = find_range_of_T(patient_info=patient_info, gap=g)
    alpha = list()  # Patient-dependant delay list
    l = list()  # Patient-dependant 2nd feasible interval length list
    for patient in patient_info:
        alpha.append(patient[2])
        l.append(patient[3])

    # Create parameters a_it
    a = {}
    for i in range(len(patient_info)):
        ri1 = patient_info[i][0]
        di1 = patient_info[i][1]
        for t in T:
            if ri1 <= t + 1 <= di1 - p1 + 1:
                a[(i, t)] = 1
            else:
                a[(i, t)] = 0
    write_memory_used(filename, "a_it", process.memory_info().rss)

    # Create decision variables.
    x = {}  # x[j] = 1 if hospital j is used for vaccination
    for j in n:
        x[j] = model.NewBoolVar('x_%i' % (j + 1))
    write_memory_used(filename, "x", process.memory_info().rss)
    write_time_used(filename, "x", time.perf_counter() - start)

    y = {}  # y[(i,t,j)] = 1 if patient i gets 1st dose vaccinated at time t in hospital j
    for i in n:
        for t in T:
            for j in n:
                y[(i, t, j)] = model.NewBoolVar('y_%i,%i,%i' % (i + 1, t + 1, j + 1))
    write_memory_used(filename, "y", process.memory_info().rss)
    write_time_used(filename, "y", time.perf_counter() - start)

    z = {}  # z[(i,t,j)] = 1 if patient i gets 2nd dose vaccinated at time t in hospital j
    for i in n:
        for t in T:
            for j in n:
                z[(i, t, j)] = model.NewBoolVar('z_%i,%i,%i' % (i + 1, t + 1, j + 1))
    write_memory_used(filename, "z", process.memory_info().rss)
    write_time_used(filename, "z", time.perf_counter() - start)

    # Add the constraints to the model
    # constraint 1
    for i in n:
        model.Add(sum(y[(i, t, j)] for t in T for j in n) == 1)
    write_memory_used(filename, "c1", process.memory_info().rss)
    write_time_used(filename, "c1", time.perf_counter() - start)

    # constraint 2
    for i in n:
        model.Add(sum(z[(i, t, j)] for t in T for j in n) == 1)
    write_memory_used(filename, "c2", process.memory_info().rss)
    write_time_used(filename, "c2", time.perf_counter() - start)

    # constraint 3 & 4
    print('C3')
    for t in T:
        for j in n:
            model.Add(sum(y[(i, k, j)] for i in n for k in range(max(t - p1 + 1, 0), t + 1)) +
                      sum(z[(i, k, j)] for i in n for k in range(max(t - p2 + 1, 0), t + 1)) <= 1)
    write_memory_used(filename, "c34", process.memory_info().rss)
    write_time_used(filename, "c34", time.perf_counter() - start)
    # constraint 5
    print('C5')
    for i in n:
        for t in T:
            for j in n:
                model.Add(y[(i, t, j)] <= a[(i, t)] * x[j])
                # print(y[(i, t, j)] <= a[(i, t)] * x[j])
    write_memory_used(filename, "c5", process.memory_info().rss)
    write_time_used(filename, "c5", time.perf_counter() - start)
    # constraint 6
    print('C6')
    for t in T:
        for i in n:
            if t - p1 - g - alpha[i] < 0:
                model.Add(sum(z[(i, t, j)] for j in n) == 0)
            else:
                model.Add(sum(z[(i, t, j)] for j in n) <= sum(
                    y[(i, k, j)] for k in range(max(t - p1 - g - alpha[i] - l[i] + p2, 0), t - p1 - g - alpha[i] + 1)
                    for j in n))
    write_memory_used(filename, "c6", process.memory_info().rss)
    write_time_used(filename, "c6", time.perf_counter() - start)
    # constraint 7
    print('C7')
    for i in n:
        for t in T:
            for j in n:
                model.Add(z[(i, t, j)] <= x[j])
    write_memory_used(filename, "c7", process.memory_info().rss)
    write_time_used(filename, "c7", time.perf_counter() - start)

    # constraint 8
    print('C8')
    for j in range(N - 1):
        model.Add(x[j + 1] <= x[j])
    write_memory_used(filename, "c8", process.memory_info().rss)
    write_time_used(filename, "c8", time.perf_counter() - start)

    # Objective function
    model.Minimize(sum(x[j] for j in n))
    write_memory_used(filename, "obj", process.memory_info().rss)
    write_time_used(filename, "obj", time.perf_counter() - start)

    # Creates the solver and solve.
    solver = cp_model.CpSolver()
    solver.Solve(model)
    write_memory_used(filename, "sol", process.memory_info().rss)
    write_time_used(filename, "sol", time.perf_counter() - start)

    for i in n:  # For each patient
        hospital_dose_1 = None
        hospital_dose_2 = None
        time1 = None
        time2 = None
        for t in T:  # For each time slot
            for j in n:  # For each hospital
                if solver.BooleanValue(y[(i, t, j)]):
                    hospital_dose_1 = j + 1
                    time1 = t + 1

                if solver.BooleanValue(z[(i, t, j)]):
                    hospital_dose_2 = j + 1
                    time2 = t + 1
        write_to_file_patient_list(filename, time1, hospital_dose_1, time2, hospital_dose_2)
        print(time1, hospital_dose_1, time2, hospital_dose_2)
    write_to_file_hospitals_needed(filename, int(solver.ObjectiveValue()))
    print(int(solver.ObjectiveValue()))
    move_file_to_passed_cases(filename)

