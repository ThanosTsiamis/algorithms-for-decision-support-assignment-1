import glob
import os

from offline_case.create_random_test_case_instances import create_random_test_case_instances
from offline_case.ilp_modelling import ilp_solver


def iterate_through_every_case():
    path = "../Test_Cases/Offline"
    os.chdir(path)
    for file in glob.glob("*.txt"):
        ilp_solver(filename=file)


def compute_specific_case(filename, path=None):
    if path is None:
        path = "../Test_Cases/Offline"
    os.chdir(path)
    ilp_solver(filename=filename)


if __name__ == "__main__":
    print("Program Starts:...")
    # for i in range(1,26):
    #     create_random_test_case_instances(nr_of_files_to_be_created=1, nr_of_patients=25, processing_time_1=1,
    #                                   processing_time_2=i, gap=0, first_available_time_slot=None,
    #                                   length_first_timeslot=None, delay=None,
    #                                   length_of_second_interval=None)

    compute_specific_case("45.txt")
    #iterate_through_every_case()
