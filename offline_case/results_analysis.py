import os
import glob

import matplotlib.pyplot as plt

import pandas as pd


def plot_time_intervals():
    path = "../Test_Cases/Offline/Output/Memory_Usage"
    os.chdir(path)
    labels = list()
    for file in glob.glob("*.txt"):
        if file.startswith("Time"):
            data = pd.read_csv(file, header=None)
            plt.plot(data.iloc[:-1, 0], data.iloc[:-1, 1])
            labels.append(file.split("_")[-1].split(".")[0])
        plt.legend(labels, ncol=4, loc='upper center', title='filename',
                   bbox_to_anchor=[0.5, 1.1],
                   columnspacing=1.0, labelspacing=0.0,
                   handletextpad=0.0, handlelength=1.5,
                   fancybox=True, shadow=True)
    plt.yscale('log')
    plt.xlabel("Variable -  constraint")
    plt.ylabel("Cummulative time taken in seconds")
    plt.show()


def plot_memory_intervals():
    path = "../Test_Cases/Offline/Output/Memory_Usage"
    os.chdir(path)
    labels = list()
    for file in glob.glob("*.txt"):
        if file.startswith("metrics"):
            data = pd.read_csv(file, header=None)
            plt.plot(data.iloc[:-1, 0], data.iloc[:-1, 1])
            labels.append(file.split("_")[-1].split(".")[0])
        plt.legend(labels, ncol=4, loc='upper center', title='filename',
                   bbox_to_anchor=[0.5, 1.1],
                   columnspacing=1.0, labelspacing=0.0,
                   handletextpad=0.0, handlelength=1.5,
                   fancybox=True, shadow=True)
    # plt.yscale('log')
    plt.xlabel("Variable - constraint")
    plt.ylabel("RAM used in 10xGBs ")
    plt.show()


def draw_subplots():
    path = R"../Test_Cases/Offline/passed_cases"
    os.chdir(path)
    p1_list = list()
    p2_list = list()
    g_list = list()
    for file in glob.glob("*.txt"):
        with open(file) as f:
            text = f.readlines()
            p1_list.append(int(text[0].rstrip()))
            p2_list.append(int(text[1].rstrip()))
            g_list.append(int(text[2].rstrip()))

    os.chdir("..")
    os.chdir("./Output")

    hospital_list = list()
    patient_list = list()
    for file in glob.glob("*.txt"):
        with open(file) as f:
            text = f.readlines()
            hospitals = int(text[-1])
            nr_patients = len(text) - 1
            hospital_list.append(hospitals)
            patient_list.append(nr_patients)
    # dataframe = pd.DataFrame(
    #     {'p1': p1_list, 'p2': p2_list, 'gap': g_list, 'nr_hospitals': hospital_list, 'patient_nr': patient_list})
    plt.scatter(p2_list, hospital_list, color="red",
                label='scatter')
    plt.ylabel("hospitals needed")
    plt.xlabel("p2")
    plt.title("p2/Hospitals needed")

    # fig, axs = plt.subplots(2, 2)
    # axs[0, 0].scatter(patient_list, hospital_list, color="red")
    # # list_of_coef = np.polyfit(patient_list, hospital_list, 1)
    # # axs[0, 0].plot(patient_list, list_of_coef[0] * np.asarray(patient_list) + list_of_coef[1])
    # axs[0, 0].set_title('patients vs hospitals needed')
    #
    # axs[0, 1].scatter(p1_list, hospital_list, color="blue")
    # # list_of_coef= np.polyfit(np.asarray(p1_list), np.asarray(hospital_list), 1)
    # # axs[0, 1].plot(patient_list, list_of_coef[0] * np.asarray(p1_list) + list_of_coef[1])
    # axs[0, 1].set_title('p1 vs hospitals needed')
    #
    # axs[1, 0].scatter(p2_list, hospital_list, color="orange")
    # # list_of_coef = np.polyfit(p2_list, hospital_list, 1)
    # # axs[1, 0].plot(patient_list, list_of_coef[0] * np.asarray(p2_list) + list_of_coef[1])
    # axs[1, 0].set_title('p2 vs hospitals needed')
    #
    # axs[1, 1].scatter(g_list, hospital_list, color="black")
    # # list_of_coef = np.polyfit(g_list, hospital_list, 1)
    # # axs[1, 1].plot(patient_list,list_of_coef[0] * np.asarray(g_list) + list_of_coef[1])
    # axs[1, 1].set_title('gap vs hospitals needed')

    # for ax in axs.flat:
    #     ax.set(xlabel='123', ylabel='hospitals needed')

    # Hide x labels and tick labels for top plots and y ticks for right plots.
    # for ax in axs.flat:
    #     ax.label_outer()
    plt.show()


if __name__ == "__main__":
    # draw_subplots()
    # plot_time_intervals()
    plot_memory_intervals()
