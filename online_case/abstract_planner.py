
from abc import ABC, abstractmethod
from os import fstat

class Planner(ABC):
    timeStart = 0                   # lowest possible timeslot, i.e. 0 or 1 based schedule
    machineHorizon = 0xffffffff     # upper bound on the schedule, latest timeslot for which planner supports scheduling

    def __init__(self, p1, p2, g):
        self.vaccineSchedule = []               # stores the planned vaccinations per patient
        self.freeSchedule = []                  # stores remaining unoccupied intervals per hospital
        self.p1 = p1
        self.p2 = p2
        self.g  = g
        self.smallestTime = min(p1, p2)         # determine the smallest duration of the vaccines
        self.biggestTime  = max(p1, p2)         # determine the largest duration of the vaccines
        self.add_new_hospital()                     # we need at least one machine for scheduling

    def __str__(self):
        string = ""
        for vaccine in self.vaccineSchedule:
            (timeslot1, machine1, timeslot2, machine2) = vaccine
            string = string + ("{0}, {1}, {2}, {3}\n".format(timeslot1, machine1+1, timeslot2, machine2+1))
        string = string + str(len(self.freeSchedule))
        return string


    @abstractmethod
    def plan_new_patient(self, schedule):
        pass


    def add_new_hospital(self):
        self.freeSchedule.append([(self.timeStart, self.machineHorizon)])     # each new hospital starts with the largest possible interval (with an arbitrarily set upperbound)

    def collect_free_schedule(self, patientInterval, vaccineTime):
        """
        Searches and collects all available intervals on all active machines in which the current patient could be scheduled. \n
        :patientInterval: interval in which the patient is able to be vaccinated. \n
        :vaccinetime: time needed for the patient to 
        """
        availableIntervals = []
        for i in range(len(self.freeSchedule)):             # iterate through the different hospitals in search for valid intervals
            for j in range(len(self.freeSchedule[i])):      # iterate through the (possibly multiple) intervals in search of valid intervals
                if self.is_valid_subset(self.freeSchedule[i][j], patientInterval):
                    availableIntervals.append((self.freeSchedule[i][j], i))
                else:
                    intersects = self.is_valid_intersection(self.freeSchedule[i][j], patientInterval, vaccineTime)
                    if intersects:
                        availableIntervals.append((self.freeSchedule[i][j], i))
        
        return availableIntervals
    

    def get_all_valid_subsets(self, patientInterval):
        availableIntervals = []
        for i in range(self.freeSchedule.count()):           # iterate through the different hospitals in search for valid intervals
            for j in range(self.freeSchedule[i].count):      # iterate through the (possibly multiple) intervals in search of valid intervals
                if self.is_valid_subset(self.freeSchedule[i][j], patientInterval):
                    availableIntervals.append((self.freeSchedule[i][j], i))
        return availableIntervals


    def is_valid_subset(self, scheduleInterval, patientInterval):
        """
        Evaluates if the patient interval is a subset of the freeSchedule interval. \n
        :scheduleInterval: interval of the freeSchedule datastructure. \n
        :patientInterval: interval of the patient that needs to be planned. \n
        :returns: if the patientInterval is a subset of the scheduleInterval or not.
        """
        fStart, fEnd = scheduleInterval
        pStart, pEnd = patientInterval
        return (fStart <= pStart) and (fEnd >= pEnd)


    def get_all_valid_intersections(self, patientInterval, vaccineTime):
            availableIntervals = []
            for i in range(len(self.freeSchedule)):                # iterate through the different hospitals in search for valid intervals
                for j in range(len(self.freeSchedule[i])):         # iterate through the (possibly multiple) intervals in search of valid intervals
                    if self.is_valid_intersection(self.freeSchedule[i][j], patientInterval, vaccineTime):
                        availableIntervals.append((self.freeSchedule[i][j], i))
            return availableIntervals


    def is_valid_intersection(self, scheduleInterval, patientInterval, vaccineTime):
        """
        Evaluates if a the patient and freeSchedule intervals overlap sufficiently to schedule a vaccination. \n
        :scheduleInterval: interval of the freeSchedule datastructure. \n
        :patientInterval: interval of the patient that needs to be planned. \n
        :vaccineTime: time required to be vacant for the patient to be vaccinated, p1 or p2. \n
        :returns: The interval in which the vaccination can be scheduled and completed, or (-1,-1) if no such interval exists.
        """
        fStart, fEnd = scheduleInterval
        pStart, pEnd = patientInterval
        if (pStart + vaccineTime - 1 <= fEnd and pStart >= fStart):     # in this case the upper bound of freeSchedule intersects with the patient
            return True                                                 # viable interval is therefore: (c, "upper bound of free schedule"-"time taken to vaccinate")
        elif (pEnd - vaccineTime >= fStart and pEnd <= fEnd):           # in this case the lower bound of freeSchedule intersects with the patient
            return True                         # viable interval is lower bound of freeSchedule and upper bound of patient 
        else:
            return False                        # returns False if there is no possibility for planning a vaccine in the overlapping intervals 


    def place_patient(self, machine1, start1, machine2, start2):
        """
        Will place the patient at the specified machine at the specified time, for both vaccines respectively.
        """
        self.update_freeSchedule(machine1, start1, self.p1)
        self.update_freeSchedule(machine2, start2, self.p2)


    def update_freeSchedule(self, machine, pStart, duration):
        """
        Updates the free schedule by removing the used timeslots from the machine where a dose is planned.
        """
        newSchedule = []
        for i in range(len(self.freeSchedule[machine])):
            fStart, fEnd = self.freeSchedule[machine][i]                # unpack start and end of the free schedule's interval tuples
            if(fStart >= pStart+duration):
                newSchedule.append(self.freeSchedule[machine][i])       # leave the free interval unchanged if it starts is later than the planned vaccine ends
                continue
            elif(pStart >= fEnd):
                newSchedule.append(self.freeSchedule[machine][i])       # leave the free interval unchanged if it ends is earlier than the planned vaccine starts
                continue
            else:                                                           # interval that shares its range with the vaccine timeslot
                if(pStart - fStart >= self.smallestTime):                   # only add the new interval if it is big enough to fit another future vaccine
                    newSchedule.append((fStart, pStart - 1))                # new interval before the vaccine
                if(fEnd - (pStart + duration) >= self.smallestTime):        # only add the new interval if it is big enough to fit another future vaccine
                    newSchedule.append((pStart + duration, fEnd))           # new interval after the vaccine
                continue
        self.freeSchedule[machine] = newSchedule                            # replaces the old schedule with the updated one
        return 
