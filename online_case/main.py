import argparse, os, re, random
import time
from online_planner import SequentialVaccinesPlanner, IntegratedVaccinesPlanner

instancePath = "../Test_Cases/"         #Path searched for files
outputPath   = "../Test_Cases/Online/"  #Path used for output

# Some command line functionality
parser = argparse.ArgumentParser(description="2-Dose Vaccine Planner")
parser.add_argument("-p", "--planner", type=str, default="S", help="Type of planner to use: S: sequential, I: integrated")
parser.add_argument("-t", "--type", type=str, default="Online", help="Type of the instance format: Online uses online format, Offline allows to plan offline instances as list heuristic")
parser.add_argument("-f", "--files", type=str, default=None, nargs="*", help= "Only creates a planning for the files specified")
parser.add_argument("-i", "--instance", type=str, default="files", help="files for running Test_Cases/Online instances.\n random for randomly generated instances.\n benchmark for comparing sequential and integrated planners on random instances")

args = parser.parse_args()

def online_plan_from_file(input, output):
    """
    Plans a schedule based on an input file
    """
    start = time.perf_counter()

    lines = input.read().splitlines()
    p1 = int(lines[0])
    p2 = int(lines[1])
    g = int(lines[2])

    # Decide which planner to use
    if(args.planner == "I"):
        schedule = IntegratedVaccinesPlanner(p1, p2, g, output)
    else:
        schedule = SequentialVaccinesPlanner(p1, p2, g, output)

    # start adding patients from the file to the queue until termination symbol "x" is read
    currentLine = 3
    while(lines[currentLine] != "x"):
        (release, deadline, pause, intervalLength) = [int(num, base=10) for num in lines[currentLine].split(",")]
        schedule.plan_new_patient(release, deadline, pause, intervalLength)
        currentLine = currentLine + 1
    
    duration = time.perf_counter() - start
    output.write("Total instance time: {0}\n".format(duration))
    output.write(str(schedule))
    return

def offline_plan_from_file(input, output):
    """
    Plans a schedule based on an input file, offline files are treated as if they were online instances
    """
    start = time.perf_counter()

    lines = input.read().splitlines()
    p1 = int(lines[0])
    p2 = int(lines[1])
    g = int(lines[2])
    n = int(lines[3])

    # Decide which planner to use
    if(args.planner == "I"):
        schedule = IntegratedVaccinesPlanner(p1, p2, g, output)
    else: 
        schedule = SequentialVaccinesPlanner(p1, p2, g, output)

    # start adding patients from the file to the queue until termination symbol "x" is read
    currentLine = 4
    for i in range(n):
        (release, deadline, pause, intervalLength) = [int(num, base=10) for num in lines[currentLine].split(",")]
        schedule.plan_new_patient(release, deadline, pause, intervalLength)
        currentLine = currentLine + 1
    
    duration = time.perf_counter() - start
    output.write("Total instance time: {0}\n".format(duration))
    output.write(str(schedule))
    return

def plan_from_random(patients, output, repeats=100, p1Lim=100, p2Lim=100, gLim=100, r1Lim=100, dLim=100, xLim=100, ellLim=100):
    """
    Randomly generates and solves a number of problem instances based on a set of parameters
    """
    maxTime = -1
    totalTime = 0
    for i in range(repeats):  # run repeats-nr of random instances
        start = time.perf_counter()
        
        p1 = random.randint(1, p1Lim)  # randomly choose a value for p1 in the range [1, p1Lim]
        p2 = random.randint(1, p2Lim)  # randomly choose a value for p2 in the range [1, p2Lim]
        g = random.randint(1, gLim)  # randomly choose a value for g in the range [1, gLim]

        # Decide which planner to use
        if(args.planner == "I"):
            schedule = IntegratedVaccinesPlanner(p1, p2, g, None)
        else: 
            schedule = SequentialVaccinesPlanner(p1, p2, g, None)

        for j in range(patients):
            release = random.randint(1, r1Lim)  # randomly choose a value for this patient's release, 1-based scheduling
            deadline = release+p1+random.randint(0, dLim)  # randomly choose a value for this patient's deadline
            pause = random.randint(0, xLim)  # randomly choose a value for this patient's deadline
            intervalLength = p2 + random.randint(0, ellLim)  # randomly choose a value for this patient's second interval length
            
            schedule.plan_new_patient(release, deadline, pause, intervalLength)
        
        duration = time.perf_counter() - start
        if(duration > maxTime):
            maxTime = duration
        totalTime = totalTime + duration
    
    output.write("{0};{1};{2};{3}\n".format(patients, repeats, totalTime, maxTime))
    return

def integrated_vs_sequential_benchmark(patients, output, repeats=50, p1Lim=100, p2Lim=100, gLim=100, r1Lim=100, dLim=100, xLim=100, ellLim=100):
    seed = random.randint(0, 0xffffffff)  # store randomly generated seed to ensure input for both planners are identical

    ### Sequential planner
    sequentialMaxTime = -1
    sequentialTotalTime = 0
    sequentialMaxMachines = -1
    sequentialTotalMachines = 0

    random.seed(seed) # set random to seed
    for i in range(repeats):
        p1 = random.randint(1, p1Lim)  # randomly choose a value for p1 in the range [1, p1Lim]
        p2 = random.randint(1, p2Lim)  # randomly choose a value for p2 in the range [1, p2Lim]
        g = random.randint(1, gLim)  # randomly choose a value for g in the range [1, gLim]
        
        start = time.perf_counter()  # log time required to solve the instance
        schedule = SequentialVaccinesPlanner(p1, p2, g, None)

        for j in range(patients):
            release = random.randint(1, r1Lim)  # randomly choose a value for this patient's release, 1-based scheduling
            deadline = release+p1+random.randint(0, dLim)+1  # randomly choose a value for this patient's deadline
            pause = random.randint(0, xLim)  # randomly choose a value for this patient's deadline
            intervalLength = p2 + random.randint(1, ellLim)  # randomly choose a value for this patient's second interval length
            
            schedule.plan_new_patient(release, deadline, pause, intervalLength)
        
        duration = time.perf_counter() - start
        if(duration > sequentialMaxTime):
            sequentialMaxTime = duration
        if(len(schedule.freeSchedule) > sequentialMaxMachines):
            sequentialMaxMachines = len(schedule.freeSchedule)
        sequentialTotalTime = sequentialTotalTime + duration
        sequentialTotalMachines = sequentialTotalMachines + len(schedule.freeSchedule)

    ### Integrated planner
    integratedMaxTime = -1
    integratedTotalTime = 0
    integratedMaxMachines = -1
    integratedTotalMachines = 0
    
    random.seed(seed) # reset random to the same seed used for sequential, resulting in the same patients being presented
    for i in range(repeats):
        start = time.perf_counter()  # log time required to solve the instance
        schedule = IntegratedVaccinesPlanner(p1, p2, g, None)

        for j in range(patients):
            release = random.randint(1, r1Lim)  # randomly choose a value for this patient's release, 1-based scheduling
            deadline = release+p1+random.randint(0, dLim)  # randomly choose a value for this patient's deadline
            pause = random.randint(0, xLim)  # randomly choose a value for this patient's deadline
            intervalLength = p2 + random.randint(0, ellLim)  # randomly choose a value for this patient's second interval length
            schedule.plan_new_patient(release, deadline, pause, intervalLength)
        
        duration = time.perf_counter() - start
        if(duration > integratedMaxTime):
            integratedMaxTime = duration
        if(len(schedule.freeSchedule) > integratedMaxMachines):
            integratedMaxMachines = len(schedule.freeSchedule)
        integratedTotalTime = integratedTotalTime + duration
        integratedTotalMachines = integratedTotalMachines + len(schedule.freeSchedule)
    
    output.write("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};\n".format(patients, repeats, seed, 
                    sequentialTotalTime, sequentialMaxTime, sequentialTotalMachines, sequentialMaxMachines,
                    integratedTotalTime, integratedMaxTime, integratedTotalMachines, integratedMaxMachines
                ))


if __name__ == "__main__":
    if(args.instance == "files"): # Solve scheduling for instance files present in the /Test_Cases/Online folder
        readDirPath = instancePath + args.type + "/"
        files = args.files
        if(args.files == None):      # if no CL argument was given plan all files in directory instead
            files = os.listdir(readDirPath)
            files.pop() # remove "Offline" folder from list
            files.pop() # remove "Results" folder from list
            files = sorted(files, key=lambda a: int(re.split("-|\.", a)[0]))            # sort files in order of nr. of patients
        
        for filename in files:
            print(filename)
            if args.type == "Online":
                with open(readDirPath + filename) as input:
                    with open(outputPath + "Results/" + args.planner + filename, "w") as output:     # prepend S/I to the filename to distinguish which planner was used
                        #try:
                            online_plan_from_file(input, output)
                        #except Exception:   # Continue with the next file if any errors occur
                        #    output.write("An exception has occured while planning this instance.")
                        #    continue
            elif args.type == "Offline":
                with open(readDirPath + filename) as input:
                    with open(outputPath + "OfflineResults/" + args.planner + filename, "w") as output:     # prepend S/I to the filename to distinguish which planner was used
                        #try:
                            offline_plan_from_file(input, output)
                        #except Exception:   # Continue with the next file if any errors occur
                        #    output.write("An exception has occured while planning this instance.")
                        #    continue
            else:
                print("Unknown argument for -t --type, please use either Online or Offline (case sensitive)")
    
    elif(args.instance == "random"): # Solve scheduling for a randomly generated instance
        with open(outputPath + args.planner + "randoms.csv", "w") as output:
            # csv table headers
            output.write("patientnr;repeats;totalTime;maxTime\n")
            for patientNr in [10, 50, 100, 500, 1000]:
                print("{0} patients".format(patientNr))
                plan_from_random(patientNr, output)

    elif(args.instance == "benchmark"):
        with open(outputPath + "Results/bench_randoms.csv", "w") as output:
            # csv table headers
            output.write("patientnr;repeats;seed;totalTime_S;maxTime_S;totalMachines_S;maxMachines_S;totalTime_I;maxTime_I;totalMachines_I;maxMachines_I;\n") 
            for patientNr in [10, 50, 100, 500, 1000]:
                print("{0} patients".format(patientNr))
                integrated_vs_sequential_benchmark(patientNr, output)