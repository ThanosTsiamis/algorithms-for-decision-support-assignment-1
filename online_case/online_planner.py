import math, time

from abstract_planner import Planner


class IntegratedVaccinesPlanner(Planner):
    def __init__(self, p1, p2, g, output):
        start = time.perf_counter()
        Planner.__init__(self, p1, p2, g)
        self.accuracyCutOff = 0x0000ffff  # cutoff value until which the values of atan(x) will be precomputed accurately
        self.alpha = 2  # value used in the planning heuristic to reward seemless vaccine planning, should be > pi/2
        self.output = output  # file to write output to
        if(output != None):
            output.write("==Integrated Doses Vaccine Planner==\n")

        # atan causes heuristic to value increasing the size of small intervals more than larger intervals
        self.scoreTable = [math.atan(num) for num in
                           range(min(self.machineHorizon, self.accuracyCutOff))]  # precompute for speeding up find_best_start_time
        smallestDose = min(self.p1, self.p2, self.accuracyCutOff-1)
        for i in range(1, smallestDose+ 1):  # any interval length smaller than the smallest dose time is useless (except for a continuous planning where length==0)
            self.scoreTable[i] = 0
        self.scoreTable[0] = self.scoreTable[0] + self.alpha

        preprocessing = time.perf_counter() - start  # time taken to compute the scoreTable
        if(output != None):
            output.write("Machine horizon: {0}\n".format(self.machineHorizon))
            output.write("Preprocessing time: {0}\n".format(preprocessing))

    def plan_new_patient(self, start, end, delayLength, secondInterval):
        """
        Finds the best time to plan the vaccines for the patient based on the implemented heuristic
        """
        patientInterval1 = (start, end)  # interval for the first dose
        patientInterval2 = (start + self.p1 + self.g + delayLength,
                            end + self.p1 + self.g + delayLength + secondInterval)  # interval for the second dose, encompasses any possible start time for the first vaccine
        freeschedule1 = self.collect_free_schedule(patientInterval1,
                                                   self.p1)  # free intervals suited to plan the first dose
        freeschedule2 = self.collect_free_schedule(patientInterval2,
                                                   self.p2)  # free intervals suited to plan the second dose, freeschedule1 and freeschedule2 are not nessecarily disjoint

        if (freeschedule1 == [] or freeschedule2 == []):  # If there are no valid intersections left add a new machine and fetch again
            self.add_new_hospital()
            freeschedule1 = self.collect_free_schedule(patientInterval1, self.p1)
            freeschedule2 = self.collect_free_schedule(patientInterval2, self.p2)

        # plan both first and second dose at once
        timeslot1, machine1, timeslot2, machine2 = self.find_best_start_time(freeschedule1, freeschedule2,
                                                                             patientInterval1, delayLength,
                                                                             secondInterval)
        # Once an "optimal" solution has been found update the schedule with the patient's planned time/machine combinations
        self.place_patient(machine1, timeslot1, machine2, timeslot2)
        self.vaccineSchedule.append((timeslot1, machine1, timeslot2, machine2))

    def find_best_start_time(self, freeSchedule1, freeSchedule2, patientInterval, delayLength, secondInterval):
        """
        Returns the timeslot best suited to start vaccinating the patient.
        Same procedure as for the sequential planner but with optimal second dose planning integrated into the planning of the first dose.
        """
        bestScore = -1  # Best score found according to the used function
        bestTime1 = -1  # Current best time option for the first dose
        bestMachine1 = -1  # Current best machine option for the first dose
        bestTime2 = -1  # Current best time option for the second dose
        bestMachine2 = -1  # Current best machine option for the second dose

        pStart1, pEnd1 = patientInterval  # unpacking the patient interval
        for interval1 in freeSchedule1:  # check all intervals available for the first dose
            ((fStart1, fEnd1), machine1) = interval1  # unpacking the freeSchedule interval
            trueLowerBound1 = max(pStart1,
                                  fStart1)  # latest start of interval determines lower bound for planning the vaccine
            trueUpperBound1 = min(pEnd1, fEnd1) - self.p1 + 1  # earliest end of interval determines the upper bound
            if trueLowerBound1 > trueUpperBound1:
                trueUpperBound1 = trueLowerBound1
            for i in range(trueLowerBound1,
                           trueUpperBound1 + 1):  # viable range to schedule a vaccine will be end minus vaccine time (+1 for exclusive upper bound)
                earlyGap1 = i - fStart1  # length of interval before timeslot i
                lateGap1 = fEnd1 - self.p1 - i  # length of interval after timeslot i

                pStart2 = i + self.p1 + self.g + delayLength
                pEnd2 = i + self.p1 + self.g + delayLength + secondInterval - 1

                for interval2 in freeSchedule2:
                    ((fStart2, fEnd2), machine2) = interval2  # unpacking the freeSchedule interval
                    trueLowerBound2 = max(pStart2,
                                          fStart2)  # latest start of interval determines lower bound for planning the vaccine
                    trueUpperBound2 = min(pEnd2,
                                          fEnd2) - self.p2 + 1  # earliest end of interval determines the upper bound

                    if trueLowerBound2 > trueUpperBound2:
                        trueUpperBound2 = trueLowerBound2
                    for j in range(trueLowerBound2, trueUpperBound2 + 1):
                        earlyGap2 = j - fStart2  # length of interval before 2nd dose timeslot j
                        lateGap2 = fEnd2 - self.p2 - j  # length of interval after 2nd dose timeslot j

                        # flexibilityscore measures how much options the early and late gap provide for planning future vaccines
                        # atan causes heuristic to value increasing the size of small intervals more than larger intervals
                        flexibilityScore = self._flexScore(earlyGap1) + self._flexScore(lateGap1) + self._flexScore(earlyGap2) + self._flexScore(lateGap2)
                        if (flexibilityScore > bestScore):  # Update best scheduling if a better solution is found
                            bestScore = flexibilityScore
                            bestTime1 = i
                            bestMachine1 = machine1
                            bestTime2 = j
                            bestMachine2 = machine2

        return bestTime1, bestMachine1, bestTime2, bestMachine2

    def _flexScore(self, value):
        if(value < self.accuracyCutOff):
            return self.scoreTable[value]  # return flexibility value from the precomputed table if it has been calculated
        else:                           
            return (math.pi/2) # if it lies outside of the range return the constant value of atan(x) when x-->infinity (assuming this will become a constant at compile time)


class SequentialVaccinesPlanner(Planner):

    def __init__(self, p1, p2, g, output):
        start = time.perf_counter()
        Planner.__init__(self, p1, p2, g)
        self.accuracyCutOff = 0x0000ffff  # cutoff value until which the values of atan(x) will be precomputed accurately
        self.alpha = 2  # value used in the planning heuristic to reward seemless vaccine planning, should be > pi/2
        self.output = output
        if(output != None):
            output.write("==Sequential Doses Vaccine Planner==\n")


        self.scoreTable = [math.atan(num) for num in
                           range(min(self.machineHorizon, self.accuracyCutOff))]  # precompute for speeding up find_best_start_time
        smallestDose = min(self.p1, self.p2, self.accuracyCutOff-1)
        for i in range(1, smallestDose + 1):  # any interval length smaller than the smallest dose time is useless (except for a continuous planning where length==0)
            self.scoreTable[i] = 0
        preprocessing = time.perf_counter() - start  # time taken to compute the scoreTable
        
        if(output != None):
            output.write("Machine horizon: {0}\n".format(self.machineHorizon))
            output.write("Preprocessing time: {0}\n".format(preprocessing))

    def plan_new_patient(self, start, end, delayLength, secondInterval):
        """
        Finds the best time to plan the vaccines for the patient based on the implemented heuristic
        """
        ### Plan first dose
        patientInterval = (start, end)

        # Will return only the intervals that overlap with the patientinterval
        freeschedule = self.collect_free_schedule(patientInterval, self.p1)

        if (freeschedule == []):  # If there are no valid intersections left add a new machine and try again
            self.add_new_hospital()
            freeschedule = self.collect_free_schedule(patientInterval, self.p1)

        timeslot, machine = self.find_best_start_time(freeschedule, patientInterval)
        self.update_freeSchedule(machine, timeslot, self.p1)
        ### Plan second dose
        # plan the second dose based on the same heuristics as the first dose, after first dose has already been set
        patientInterval2 = (timeslot + self.p1 + self.g + delayLength, 
                            timeslot + self.p1 + self.g + delayLength + secondInterval - 1)
        # Will return only the intervals that overlap with the 2nd patientinterval
        freeschedule2 = self.collect_free_schedule(patientInterval2, self.p2)

        if (freeschedule2 == []):  # If there are no valid intersections left add a new machine and try again
            self.add_new_hospital()
            freeschedule2 = self.collect_free_schedule(patientInterval2, self.p2)

        timeslot2, machine2 = self.find_best_start_time(freeschedule2, patientInterval2)
        self.update_freeSchedule(machine2, timeslot2, self.p2)
        #self.place_patient(machine, timeslot, machine2, timeslot2)
        self.vaccineSchedule.append((timeslot, machine, timeslot2, machine2))

    def find_best_start_time(self, intervals, patientInterval):
        """
        Returns the timeslot best suited to start vaccinating the patient
        """
        bestScore = -1
        bestTime = -1
        bestMachine = -1

        pStart, pEnd = patientInterval  # unpacking the patient interval
        for interval in intervals:  # check all intervals available
            ((fStart, fEnd), machine) = interval  # unpacking the freeSchedule interval
            trueLowerBound = max(pStart,
                                 fStart)  # latest start of interval determines lower bound for planning the vaccine
            trueUpperBound = min(pEnd, fEnd) - self.p1 + 1  # earliest end of interval determines the upper bound
            if trueLowerBound > trueUpperBound:
                trueUpperBound = trueLowerBound

            for i in range(trueLowerBound,
                           trueUpperBound + 1):  # viable range to schedule a vaccine will be end minus vaccine time (+1 for exclusive upper bound)
                earlyGap = i - fStart  # length of interval before timeslot i
                lateGap = fEnd - self.p1 - i + 1  # length of interval after timeslot i

                # flexibilityscore measures how much options the early and late gap provide for planning future vaccines
                # atan causes heuristic to value increasing the size of small intervals more than larger intervals
                flexibilityScore = self._flexScore(earlyGap) + self._flexScore(lateGap)
                if (flexibilityScore > bestScore):
                    bestScore = flexibilityScore
                    bestTime = i
                    bestMachine = machine

        return bestTime, bestMachine
    
    def _flexScore(self, value):
        if(value < self.accuracyCutOff):
            return self.scoreTable[value]  # return flexibility value from the precomputed table if it has been calculated
        else:                           
            return (math.pi/2) # if it lies outside of the range return the constant value of atan(x) when x-->infinity
