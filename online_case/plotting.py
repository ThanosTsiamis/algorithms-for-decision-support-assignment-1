import os

import matplotlib.pyplot as plt
import pandas as pd


def plotgraph():
    path="../Test_Cases/Online/Results/bench_randoms.csv"
    xticks = [10, 50, 100, 500, 1000]
    xlabels = [10, 50, 100, 500, 1000]
    data  = [x/50 for x in [0.29411370000000003,0.4653556000000001,0.8877980999999977,12.186425999999955,43.242744799999855]]
    data2 = [x/50 for x in[1.3765262999999999,4.772244200000002,105.07775459999999,1003.2239703000002,3581.5915489999993]]
   
    plt.plot(xticks, data, label="Algorithm 1")
    plt.plot(xticks, data2, label="Algorithm 2")
    plt.xscale("log")
    plt.xticks(ticks=xticks, labels=xlabels)
    plt.xlabel("Patients")
    plt.ylabel("Average processing time (s)")
    plt.legend(loc="upper left")
    plt.show()



def plotMgraph():
    xticks = [10, 50, 100, 500, 1000]
    xlabels = [10, 50, 100, 500, 1000]
    data = [8, 32, 67, 311, 641]
    data2 = [7, 22, 46, 176, 344]

   
    plt.scatter(xticks, data, label="Algorithm 1")
    plt.scatter(xticks, data2, label="Algorithm 2")
    plt.xscale("log")
    plt.xticks(ticks=xticks, labels=xlabels)
    plt.xlabel("Patients")
    plt.ylabel("Maximum scheduled machines")
    plt.legend(loc="upper left")
    plt.show()

def plotOnlines():
    xlabels=["50", "60", "96-1", "96-3", "116", "1000", "5000", "10000"]
    x = [1,2,3,4,5,6,7, 8]
    data = [13, 8, 19, 26, 32, 55, 142, 26]
    data2 = [11, 8, 18, 29, 22, 51, 112, 24]
    
    plt.scatter(x, data, label="Algorithm 1")
    plt.scatter(x, data2, label="Algorithm 2")
    plt.xticks(ticks=x, labels=xlabels, rotation=45)
    plt.xlabel("Instance label")
    plt.ylabel("Scheduled machines")
    plt.legend(loc="upper left")
    plt.show()

def plotOnlinesT():
    xlabels=["50", "60", "96-1", "96-3", "116", "1000", "5000", "10000"]
    x = [1,2,3,4,5,6,7, 8]
    data = [0.0080, 0.0431, 0.0107, 0.0100, 0.0206, 0.1929, 4.9328, 37.3406]
    data2 = [0.0138, 2.6305, 0.1484, 0.0223, 0.2132, 1.3491, 120, 14.1872]
    
    plt.scatter(x, data, label="Algorithm 1")
    plt.scatter(x, data2, label="Algorithm 2")
    plt.xticks(ticks=x, labels=xlabels, rotation=45)
    plt.xlabel("Instance label")
    plt.ylim([0,120])
    plt.ylabel("Computation time")
    plt.legend(loc="upper left")
    plt.show()

plotgraph()
plotMgraph()
#plotOnlinesT()